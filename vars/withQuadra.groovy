/**
 * Implements a wrapper to handle properly loading Quadra credentials into the Jenkins user's path
 * in order to allow Quadra commands to run without additional wrappering.
 *
 * @param parameters Configuration parameters for this helper.
 * @param body Pipeline closure to execute in the context of this helper.
 */
def call(Map parameters = [:], body) {
    env.QUADRA_APISERVER = parameters.apiServer ?: 'https://api.quadra.opendns.com'
    env.QUADRA_CREDENTIALS_ID = parameters.credentialsId ?: 'quadra-bot'

    // Execute the passed DSL inside of an implicit with credentials block.
    withCredentials([
        usernamePassword(
            credentialsId: env.QUADRA_CREDENTIALS_ID,
            usernameVariable: 'QUADRA_USERNAME',
            passwordVariable: 'QUADRA_TOKEN'
        )
    ]) {
        return body.call()
    }
}
