/**
 * Implements a wrapper to handle generation of required streamline credentials into the current
 * context.
 *
 * @param body A block of Pipeline DSL to run 'inside' a streamline session.
 */
def call(Map parameters = [:], body) {
    // Set relatively acceptable defaults.
    env.STRLN_ROLE = parameters.role ?: 'engineer'
    env.STRLN_REGION = parameters.region ?: 'us-east-1'
    env.STRLN_PROFILE = parameters.profile ?: 'strln'
    env.STRLN_ACCOUNT_ID = parameters.accountId ?: '383011734660'
    env.STRLN_CREDENTIALS_ID = parameters.credentialsId ?: 'strln-bot'

    // Always try and update streamline first.
    // TODO: Fix permissions errors.
    // sh 'sudo sl upgrade'

    // Execute the passed DSL inside of an implicit with credentials block.
    withCredentials([
        usernamePassword(
            credentialsId: env.STRLN_CREDENTIALS_ID,
            usernameVariable: 'SL_PKI_REQUEST_ID',
            passwordVariable: 'SL_PKI_DEPLOY_KEY'
        )
    ]) {
        echo "STRLN: Using ${env.STRLN_CREDENTIALS_ID} for AWS account ${env.STRLN_ACCOUNT_ID}"
        sh 'sl login'
    }

    // Finally, generate an AWS session and execute the provided block.
    echo "STRLN: Using region ${env.STRLN_REGION}"
    echo "STRLN: Using role ${env.STRLN_ROLE} to generate profile named ${env.STRLN_PROFILE}"
    sh "sl aws session generate " +
        "--account-id ${env.STRLN_ACCOUNT_ID} " +
        "--region ${env.STRLN_REGION} " +
        "--profile ${env.STRLN_PROFILE} " +
        "--role-name ${env.STRLN_ROLE} 2>&1 > /dev/null"

    // Set the default AWS SDK profile to Streamline first.
    env.AWS_DEFAULT_REGION = env.STRLN_REGION
    env.AWS_DEFAULT_PROFILE = env.STRLN_PROFILE

    return body.call()
}
